//
//  Const+LogInSignUp.swift
//  MHBook
//
//  Created by Masoud Heydari on 8/12/19.
//  Copyright © 2019 Masoud Heydari. All rights reserved.
//

import UIKit

/*************************************************/
/*********** Extension for Controller ************/
/*************************************************/
extension Const.Controller {
    
    struct Welcome {
        static let welcome_to_mh_book = "Welcome to MHBook"
        static let build_your_social_network = "Build your social network\n in minutes."
        
        static let btnLogInTitle = "Log In"
        static let btnSignUpTitle = "Sign Up"
    }
    
    struct SignUp {
        static let cellId = "sign-up-cell-id"
        
    }
    
    struct LogIn {
        static let cellId = "log-in-cell-id"
        
    }
    
}

/**************************************************/
/**************  Extension for View  **************/
/**************************************************/
extension Const.View {
    
    struct SignUp {
        static let signUp = "Sign Up"
        
        static let createNewAccount = "Create new account"
        
        // text fields place holder
        static let fullNamePlaceHolder = "Full Name"
        static let phoneNumberPlaceHolder = "Phone Number"
        static let emailAddressPlaceHolder = "E-mail Address"
        static let passwordPlaceHolder = "Password"
        
        // label term of use
        static let lblTermOfUse: NSAttributedString = {
            let attrString = NSMutableAttributedString(string: "By Creating an account you agree with our ", attributes: [.font: UIFont.systemFont(ofSize: Size.View.SignUp.txtTermOfUseFont, weight: .regular)])
            
            var attributes = [NSAttributedString.Key: Any]()
            attributes[.foregroundColor] = Color.View.SignUp.lightBlue
            
            attrString.append(NSAttributedString(string: "Terms Of Use", attributes: attributes))
            return attrString
        }()
        
    }
    
    struct LogIn {
        static let signIn = "Sign In"
        
        static let emilOrPhoneNumberPlaceHolder = "E-mail or phone number"
        static let passwordPlaceHolder = "Password"
        
        static let btnLogInTitle = "Log In"
        static let btnFacebookLogInTitle = "Facebook Login"
        
        static let lblOr = "OR"
        
        
    }
}
