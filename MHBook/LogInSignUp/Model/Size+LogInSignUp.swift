//
//  Size+LogInSignUp.swift
//  MHBook
//
//  Created by Masoud Heydari on 8/12/19.
//  Copyright © 2019 Masoud Heydari. All rights reserved.
//

import UIKit

/*************************************************/
/*********** Extension for Controller ************/
/*************************************************/
extension Size.Controller {
    
    struct Welcome {
        static let txtWelcomeFont: CGFloat = 24
        static let lblWelcomeTopPaddign: CGFloat = 30
        
        static let txtSubTitleFont: CGFloat = 16
        static let lblSubTitleTopPaddign: CGFloat = 30
        
        static let btnLogInTitleFont: CGFloat = 18
        static let btnSignUpTitleFont: CGFloat = 18
        
        static let imgLogoHeight: CGFloat = 80
        static let imgLogoWidth: CGFloat = 80
        static let imgLogoTopConstant: CGFloat = {
            if UIDevice.isiPhoneXScreen {
                return 180
            } else {
                return 140
            }
        }()
        
        static let btnsStackViewSpacing: CGFloat = 30
        static let btnsStackViewSpacerViewHeight: CGFloat = 100
        static let btnsStackViewLeftPadding: CGFloat = 50
        static let btnsStackViewRightPadding: CGFloat = -50
        static let btnsStackViewTopPadding: CGFloat = 30
        
        static let btnLogInHeight: CGFloat = 60
        static let btnSignUpHeight: CGFloat = 60
        
        static let btnLogInCornerRadious: CGFloat = btnLogInHeight / 2
        static let btnSignUpCornerRadious: CGFloat = btnSignUpHeight / 2
        
        static let btnSignUpBorderHeight: CGFloat = 1
        
    }
    
    struct SignUp {
        static let cellHeight: CGFloat = Size.Window.aHeight
    }
    
    struct LogIn {
        static let cellHeight: CGFloat = Size.Window.aHeight
    }
}


/**************************************************/
/**************  Extension for View  **************/
/**************************************************/
extension Size.View {
    
    struct SignUp {
        // label create new account
        static let txtCreateNewAccountFont: CGFloat = 24
        static let lblCreateNewAccountTopPaddign: CGFloat = 50
        static let lblCreateNewAccountRightPaddign: CGFloat = -16
        static let lblCreateNewAccountLeftPaddign: CGFloat = 16

        // label terms of use
        static let txtTermOfUseFont: CGFloat = 11
        static let lblTermOfUseBottomPaddign: CGFloat = -4
        
        // btn sign up
        static let btnSignUpTitleFont: CGFloat = 16
        static let btnSignUpHeight: CGFloat = 43
        static let btnSignUpTopPadding: CGFloat = 40
        static let btnSignUpLeftPadding: CGFloat = 60
        static let btnSignUpRightPadding: CGFloat = -60
        static let btnSignUpCornerRadious: CGFloat = btnSignUpHeight / 2
        
        // text filds
        static let textFieldBorderWidth: CGFloat = 1
        static let textFieldsFont: CGFloat = 16
        static let textFieldInnerPadding: CGFloat = 18
        static let textFieldHeight: CGFloat = 45
        
        // text fields stack view
        static let textFieldsStackViewSpacing: CGFloat = 10
        static let textFieldsStackViewTopPadding: CGFloat = 50
        static let textFieldsStackViewRightPaddign: CGFloat = -30
        static let textFieldsStackViewLeftPaddign: CGFloat = 30
        
        
        
    }
    
    struct LogIn {
        // label sign in
        static let txtSignInFont: CGFloat = 24
        static let lblSignInRightPaddign: CGFloat = -16
        static let lblSignInLeftPaddign: CGFloat = 16
        static let lblSignInTopPaddign: CGFloat = 50
        
        // btn log in
        static let btnLogInTitleFont: CGFloat = 18
        static let btnLogInHeight: CGFloat = 60
        static let btnLogInCornerRadious: CGFloat = btnLogInHeight / 2
        
        // btn facebook log in
        static let btnFacebookLoginTitleFont: CGFloat = 18
        static let btnFacebookLoginHeight: CGFloat = 60
        static let btnFacebookLoginCornerRadious: CGFloat = btnFacebookLoginHeight / 2
        
        // label OR
        static let lblORFont: CGFloat = 22
        
        // text fields
        static let textFieldBorderWidth: CGFloat = 1
        static let textFieldsFont: CGFloat = 18
        static let textFieldInnerPadding: CGFloat = 20
        static let textFieldHeight: CGFloat = 60
        
        // buttons stack view
        static let btnsStackViewSpacing: CGFloat = 35
        static let btnsStackViewTopPadding: CGFloat = 40
        static let btnsStackViewRightPaddign: CGFloat = -50
        static let btnsStackViewLeftPaddign: CGFloat = 50
        
        // text fields stack view
        static let textFieldsStackViewSpacing: CGFloat = 20
        static let textFieldsStackViewTopPadding: CGFloat = 30
        static let textFieldsStackViewRightPaddign: CGFloat = -24
        static let textFieldsStackViewLeftPaddign: CGFloat = 24
    }
}
