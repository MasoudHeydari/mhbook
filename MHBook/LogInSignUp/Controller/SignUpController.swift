//
//  SignUpController.swift
//  MHBook
//
//  Created by Masoud Heydari on 8/12/19.
//  Copyright © 2019 Masoud Heydari. All rights reserved.
//

import UIKit

class SignUpController: UIViewController {
    
    let mainView = SignUpView()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    private func setupView() {
        addViews()
        addViewConstraints()
    }
    
    private func addViews() {
        view.addSubview(mainView)
        
    }
    
    private func addViewConstraints() {
        mainView.fillToSuperview()
    }
}
