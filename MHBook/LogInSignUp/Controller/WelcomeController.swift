//
//  ViewController.swift
//  MHBook
//
//  Created by Masoud Heydari on 8/12/19.
//  Copyright © 2019 Masoud Heydari. All rights reserved.
//

import UIKit

class WelcomeController: UIViewController {
    
    let imgLogo: UIImageView = {
        let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        image.constrainHeight(constant: Size.Controller.Welcome.imgLogoHeight)
        image.constrainWidth(constant: Size.Controller.Welcome.imgLogoWidth)
        image.backgroundColor = Color.Controller.Welcome.lightBlue
        image.layer.cornerRadius = 10
        
        return image
    }()
    
    let labelWelcome: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = Color.Controller.Welcome.lightBlue
        label.text = Const.Controller.Welcome.welcome_to_mh_book
        label.font = UIFont.systemFont(ofSize: Size.Controller.Welcome.txtWelcomeFont, weight: .heavy)
        label.numberOfLines = 1
        return label
    }()
    
    let labelSubTitle: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.textColor = Color.Controller.Welcome.darkBlack
        label.text = Const.Controller.Welcome.build_your_social_network
        label.font = UIFont.systemFont(ofSize: Size.Controller.Welcome.txtSubTitleFont, weight: .heavy)
        label.numberOfLines = 2
        return label
    }()
    
    let btnLogIn: UIButton = {
        let btn = UIButton(type: .system)
        btn.constrainHeight(constant: Size.Controller.Welcome.btnLogInHeight)
        btn.layer.cornerRadius = Size.Controller.Welcome.btnLogInCornerRadious
        btn.setTitle(Const.Controller.Welcome.btnLogInTitle, for: .normal)
        btn.setSystemFont(ofSize: Size.Controller.Welcome.btnLogInTitleFont, weight: .heavy)
        btn.backgroundColor = Color.Controller.Welcome.lightBlue
        btn.setTitleColor(.white, for: .normal)
        btn.addTarget(self, action: #selector(btnLogInTapped), for: .touchUpInside)
        return btn
    }()
    
    let btnSignUp: UIButton = {
        let btn = UIButton(type: .system)
        btn.constrainHeight(constant: Size.Controller.Welcome.btnSignUpHeight)
        btn.layer.cornerRadius = Size.Controller.Welcome.btnSignUpCornerRadious
        btn.setTitle(Const.Controller.Welcome.btnSignUpTitle, for: .normal)
        btn.setSystemFont(ofSize: Size.Controller.Welcome.btnSignUpTitleFont, weight: .heavy)
        btn.backgroundColor = .white
        btn.setTitleColor(Color.Controller.Welcome.lightBlack, for: .normal)
        btn.layer.borderColor = Color.Controller.Welcome.btnBorder
        btn.layer.borderWidth = Size.Controller.Welcome.btnSignUpBorderHeight
        btn.addTarget(self, action: #selector(btnSignUpTapped), for: .touchUpInside)
        return btn
    }()
    
    let btnsStackView = UIStackView(axis: .vertical, spacing: Size.Controller.Welcome.btnsStackViewSpacing)
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        let arrayViews = (self.navigationController?.navigationBar.subviews)
        if let itemView = arrayViews?[1] {
            for lbl in itemView.subviews {
                lbl.frame = CGRect(x: -25, y: lbl.frame.origin.y, width: lbl.frame.size.width, height: lbl.frame.size.height)
            }
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    private func setupView() {
        view.backgroundColor = .white
        setupNavBar()
        addViews()
        addSubViewConstraints()
    }
    
    private func setupNavBar() {
        // remove 'Back' title from backBarButtonItem
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
        hideNavigationBarBottomDivider()
        navigationController?.navigationBar.tintColor = .black
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = .white
    }
    
    private func addViews() {
        [imgLogo, labelWelcome, labelSubTitle, btnsStackView]
            .forEach{ view.addSubview($0) }
        
        btnsStackView.addArrangedSubview(views: [btnLogIn, btnSignUp])
    }
    
    private func addSubViewConstraints() {
        [imgLogo.topAnchor.constraint(equalTo: view.topAnchor, constant: Size.Controller.Welcome.imgLogoTopConstant),
         imgLogo.centerXAnchor.constraint(equalTo: view.centerXAnchor),
         
         labelWelcome.topAnchor.constraint(equalTo: imgLogo.bottomAnchor, constant: Size.Controller.Welcome.lblWelcomeTopPaddign),
         labelWelcome.centerXAnchor.constraint(equalTo: view.centerXAnchor),
         
         labelSubTitle.topAnchor.constraint(equalTo: labelWelcome.bottomAnchor, constant: Size.Controller.Welcome.lblSubTitleTopPaddign),
         labelSubTitle.centerXAnchor.constraint(equalTo: view.centerXAnchor),
         
         btnsStackView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: Size.Controller.Welcome.btnsStackViewLeftPadding),
         btnsStackView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: Size.Controller.Welcome.btnsStackViewRightPadding),
         btnsStackView.topAnchor.constraint(equalTo: labelSubTitle.bottomAnchor, constant: Size.Controller.Welcome.btnsStackViewTopPadding),
         
         ]
            .forEach{ $0.isActive = true }
        
    }
    
    // MARK :- objc functions
    @objc private func btnLogInTapped(_ sender: UIButton) {
        print("btn log in tapped!")
        let logInController = LoginController()
        navigationController?.pushViewController(logInController, animated: true)
    }
    
    @objc private func btnSignUpTapped(_ sender: UIButton) {
        print("btn sign up tapped!")
        let signUpController = SignUpController()
        navigationController?.pushViewController(signUpController, animated: true)
        
    }
}

