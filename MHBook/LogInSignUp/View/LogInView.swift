//
//  LogInSignUpView.swift
//  MHBook
//
//  Created by Masoud Heydari on 8/12/19.
//  Copyright © 2019 Masoud Heydari. All rights reserved.
//

import UIKit

class LogInView: UIView {
    
    let labelSignIn: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = Color.View.LogIn.lightBlue
        label.text = Const.View.LogIn.signIn
        label.textAlignment = .left
        label.font = UIFont.systemFont(ofSize: Size.View.LogIn.txtSignInFont, weight: .heavy)
        label.numberOfLines = 1
        return label
    }()
    
    let emailTextField: MHTextField = {
        let padding = Size.View.LogIn.textFieldInnerPadding
        let height = Size.View.LogIn.textFieldHeight
        
        let tf = MHTextField(padding: padding, height: height)
        tf.placeholder = Const.View.LogIn.emilOrPhoneNumberPlaceHolder
        tf.keyboardType = .emailAddress
        tf.constrainHeight(constant: Size.View.LogIn.textFieldHeight)

        tf.layer.borderColor = Color.View.LogIn.textFieldBorder
        tf.layer.borderWidth = Size.View.LogIn.textFieldBorderWidth
        
        return tf
    }()
    
    let passwordTextField: MHTextField = {
        let padding = Size.View.LogIn.textFieldInnerPadding
        let height = Size.View.LogIn.textFieldHeight
        
        let tf = MHTextField(padding: padding, height: height)
        tf.placeholder = Const.View.LogIn.passwordPlaceHolder
        tf.isSecureTextEntry = true
        tf.constrainHeight(constant: Size.View.LogIn.textFieldHeight)

        
        tf.layer.borderColor = Color.View.LogIn.textFieldBorder
        tf.layer.borderWidth = Size.View.LogIn.textFieldBorderWidth
        return tf
    }()
    
    let btnLogIn: UIButton = {
        let btn = UIButton(type: .system)
        btn.constrainHeight(constant: Size.View.LogIn.btnLogInHeight)
        btn.layer.cornerRadius = Size.View.LogIn.btnLogInCornerRadious
        btn.setTitle(Const.View.LogIn.btnLogInTitle, for: .normal)
        btn.setSystemFont(ofSize: Size.View.LogIn.btnLogInTitleFont, weight: .heavy)
        btn.backgroundColor = Color.View.LogIn.lightBlue
        btn.setTitleColor(.white, for: .normal)
        return btn
    }()
    
    let labelOR: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .black
        label.text = Const.View.LogIn.lblOr
        label.font = UIFont.systemFont(ofSize: Size.View.LogIn.lblORFont, weight: .regular)
        label.numberOfLines = 1
        label.textAlignment = .center
        return label
    }()
    
    let btnFacebookLogIn: UIButton = {
        let btn = UIButton(type: .system)
        btn.layer.cornerRadius = Size.View.LogIn.btnFacebookLoginCornerRadious
        btn.backgroundColor = Color.View.LogIn.darkBlue
        btn.constrainHeight(constant: Size.View.LogIn.btnFacebookLoginHeight)
        btn.setTitle(Const.View.LogIn.btnFacebookLogInTitle, for: .normal)
        btn.setTitleColor(.white, for: .normal)
        btn.setSystemFont(ofSize: Size.View.LogIn.btnFacebookLoginTitleFont, weight: .heavy)
        return btn
    }()
    
    let textFieldsStackView = UIStackView(axis: .vertical, spacing: Size.View.LogIn.textFieldsStackViewSpacing)
    let btnsStackView = UIStackView(axis: .vertical, spacing: Size.View.LogIn.btnsStackViewSpacing)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setup() {
        backgroundColor = .white
        addViews()
        addViewsConstraints()
    }
    
    private func addViews() {
        textFieldsStackView.addArrangedSubview(views: [emailTextField, passwordTextField])
        btnsStackView.addArrangedSubview(views: [btnLogIn, labelOR, btnFacebookLogIn])
        
        [labelSignIn, textFieldsStackView, btnsStackView]
            .forEach{ addSubview($0) }
    }
    
    private func addViewsConstraints() {
        [labelSignIn.leftAnchor.constraint(equalTo: leftAnchor, constant: Size.View.LogIn.lblSignInLeftPaddign),
         labelSignIn.rightAnchor.constraint(equalTo: rightAnchor, constant:  Size.View.LogIn.lblSignInRightPaddign),
         labelSignIn.topAnchor.constraint(equalTo: topAnchor, constant:  Size.View.LogIn.lblSignInTopPaddign),
         
         textFieldsStackView.leftAnchor.constraint(equalTo: leftAnchor, constant: Size.View.LogIn.textFieldsStackViewLeftPaddign),
         textFieldsStackView.rightAnchor.constraint(equalTo: rightAnchor, constant: Size.View.LogIn.textFieldsStackViewRightPaddign),
         textFieldsStackView.topAnchor.constraint(equalTo: labelSignIn.bottomAnchor, constant: Size.View.LogIn.textFieldsStackViewTopPadding),
         
         btnsStackView.leftAnchor.constraint(equalTo: leftAnchor, constant: Size.View.LogIn.btnsStackViewLeftPaddign),
         btnsStackView.rightAnchor.constraint(equalTo: rightAnchor, constant: Size.View.LogIn.btnsStackViewRightPaddign),
         btnsStackView.topAnchor.constraint(equalTo: textFieldsStackView.bottomAnchor, constant:  Size.View.LogIn.btnsStackViewTopPadding),
         
         ].forEach{ $0.isActive = true }
    }
    
    // MARK :- objc functions
    @objc private func btnLogInTapped(_ sender: UIButton) {
        print("btn log in tapped!")
    }
    
    @objc private func btnFacebookLogInTapped(_ sender: UIButton) {
        print("btn facebook login tapped!")
    }
    
}
