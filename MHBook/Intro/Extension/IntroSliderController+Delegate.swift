//
//  IntroSliderController+Delegate.swift
//  MHBook
//
//  Created by Masoud Heydari on 8/13/19.
//  Copyright © 2019 Masoud Heydari. All rights reserved.
//

import UIKit

extension IntroSliderController: SnappingCollectionViewFlowLayoutDelegate {
    func indexForCurrentItem(index: Int) {
        self.pageControl.currentPage = index
        if canShowBtnSkip(index: index) {
            // show 'Got it' button
            btnGotIt.isHidden = false
        } else {
            // hide 'Got it' button
            btnGotIt.isHidden = true
        }
    }
    
    private func canShowBtnSkip(index: Int) -> Bool {
        return index == self.pages.count - 1
    }
}
