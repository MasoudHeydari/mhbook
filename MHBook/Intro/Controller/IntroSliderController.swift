//
//  IntroSliderController.swift
//  MHBook
//
//  Created by Masoud Heydari on 8/13/19.
//  Copyright © 2019 Masoud Heydari. All rights reserved.
//

import UIKit

class IntroSliderController: HorizontalSnappingCollectionViewController {
    
    let pages: [IntroPageModel]
    
    let pageControl: UIPageControl = {
        let page = UIPageControl()
        page.translatesAutoresizingMaskIntoConstraints = false
        page.currentPage = 0
        page.pageIndicatorTintColor = Color.Controller.IntroSlider.pageControlPageIndicator
        page.currentPageIndicatorTintColor = .white
        return page
    }()
    
    let btnGotIt: UIButton = {
        let btn = UIButton(type: .system)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.isHidden = true
        
        btn.contentEdgeInsets = Size.Controller.IntroSlider.btnGotItInset
        btn.setTitle(Const.Controller.IntroSlider.btnGotItTitle, for: .normal)
        btn.setTitleColor(.white, for: .normal)
        btn.titleLabel?.font = UIFont.systemFont(ofSize: Size.Controller.IntroSlider.btnGotITFont, weight: .heavy)
        
        let btnSize = btn.sizeThatFits(.zero)
        btn.constrainWidth(constant: btnSize.width)
        
        btn.addTarget(self, action: #selector(btnGotItTapped), for: .touchUpInside)
        return btn
    }()
    
    init(pages: [IntroPageModel]) {
        self.pages = pages
        super.init()
        setupPageControl()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        addView()
        addViewsConstriants()
    }
    
    private func setupPageControl() {
        pageControl.numberOfPages = self.pages.count
    }
    
    private func setupView() {
        setupCollectionView()
    }
    
    private func addView() {
        [btnGotIt, pageControl].forEach{ view.addSubview($0) }
    }
    
    private func addViewsConstriants() {
        let safeArea = view.safeAreaLayoutGuide
        
        [pageControl.bottomAnchor.constraint(equalTo: safeArea.bottomAnchor, constant: Size.Controller.IntroSlider.pageControlBottomPadding),
         pageControl.centerXAnchor.constraint(equalTo: view.centerXAnchor),
         
         btnGotIt.rightAnchor.constraint(equalTo: view.rightAnchor),
         btnGotIt.bottomAnchor.constraint(equalTo: safeArea.bottomAnchor, constant: Size.Controller.IntroSlider.btnGotItBottomPadding),]
            .forEach{ $0.isActive = true }
    }
    
    private func setupCollectionView() {
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.backgroundColor = Color.Controller.IntroSlider.cellbackground
        collectionView.register(IntroCell.self, forCellWithReuseIdentifier: Const.Controller.IntroSlider.cellId)
        setupCollectionViewLayout()
    }
    
    private func setupCollectionViewLayout() {
        guard let layout = collectionView.collectionViewLayout as? SnappingCollectionViewFlowLayout else { return }
        layout.delegate = self
    }
    
    // MARK:- Objc Function
    @objc private func btnGotItTapped() {
        // got welcome controller
        let welcomeController = WelcomeController()
        let navWelcomeController = UINavigationController(rootViewController: welcomeController)
        present(navWelcomeController, animated: true) {
            MainConfig.shared.firstLaunchFinished()
        }
    }
}

