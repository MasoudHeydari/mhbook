//
//  IntroPageModel.swift
//  MHBook
//
//  Created by Masoud Heydari on 8/13/19.
//  Copyright © 2019 Masoud Heydari. All rights reserved.
//

import UIKit

struct IntroPageModel {
    let image: UIImage?
    let title: String
    let subTitle: String
}
