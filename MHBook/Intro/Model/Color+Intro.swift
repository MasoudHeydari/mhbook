//
//  Color+Intro.swift
//  MHBook
//
//  Created by Masoud Heydari on 8/13/19.
//  Copyright © 2019 Masoud Heydari. All rights reserved.
//

import UIKit

/*************************************************/
/*********** Extension for Controller ************/
/*************************************************/
extension Color.Controller {
    
    struct IntroSlider {
        static let cellbackground = UIColor(r: 78, g: 116, b: 217)
        static let pageControlPageIndicator = UIColor(white: 1, alpha: 0.5)
    }
    
}

/**************************************************/
/**************  Extension for View  **************/
/**************************************************/
extension Color.View {
    
    struct IntroCell {
        static let cellbackground = UIColor(r: 78, g: 116, b: 217)
    }
    
}
