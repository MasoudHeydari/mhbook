//
//  onst+FriendShip.swift
//  MHBook
//
//  Created by Masoud Heydari on 8/14/19.
//  Copyright © 2019 Masoud Heydari. All rights reserved.
//

import UIKit

/*************************************************/
/*********** Extension for Controller ************/
/*************************************************/
extension Const.Controller {
    struct FriendShip {
        static let navTitle = "Friends"
    }
}
