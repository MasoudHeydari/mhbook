//
//  Const+Home.swift
//  MHBook
//
//  Created by Masoud Heydari on 8/14/19.
//  Copyright © 2019 Masoud Heydari. All rights reserved.
//

import UIKit

/*************************************************/
/*********** Extension for Controller ************/
/*************************************************/
extension Const.Controller {
    struct Home {
        // navigation title
        static let navTitle = "Feed"
        // header cell id
        static let headerCellId = "feed-header-cell-id"
        // feed cell id
        static let homeFeedCellId = "feed-cell-id"
        
    }
    
    struct Story {
        // story cell id
        static let storyCellId = "story-cell-id"
    }
    
    struct DetailsPost {
        // navigation title
        static let navTitle = "Details Post"
        // header cell id
        static let headerCellId = "details-post-header-cell-id"
        // feed cell id
        static let detailsPostCellId = "details-cell-id"
        // feed cell id
        static let homeFeedCellId = "feed-cell-id"
        
    }
    
    
}
