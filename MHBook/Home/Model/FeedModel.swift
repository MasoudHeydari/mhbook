//
//  FeedModel.swift
//  MHBook
//
//  Created by Masoud Heydari on 8/18/19.
//  Copyright © 2019 Masoud Heydari. All rights reserved.
//

import UIKit

struct FeedModel {
    let imgProfile: UIImage
    let userName: String
    let description: String
    let location: String
    let date: String
    var isLieked: Bool
    let likes: Int
    let comments: Int
    let images: [UIImage] // must be [String]
    var currentImage: UIImage
    var descriptionTextVeiwTextContainerMode: NSLineBreakMode
    var isDescriptionTextViewHeightActive: Bool
    
    func getCurrentImage() -> UIImage {
        return currentImage
    }
    
    mutating func setCurrentImage(currentImage: UIImage) {
        self.currentImage = currentImage
    }
    
}

func getFeedModels() -> [FeedModel] {
    
    // feed 1
    let feed1 = FeedModel(imgProfile: #imageLiteral(resourceName: "mas2"), userName: "Masoud Heydari", description: "Often times when using a UITableVie", location: "Tehran, Iran", date: "2 days ago", isLieked: true, likes: 12, comments: 312, images: [UIImage(named: "mas1")!, UIImage(named: "mas2")!, UIImage(named: "mas3")!], currentImage: UIImage(named: "mas1")!, descriptionTextVeiwTextContainerMode: .byTruncatingTail, isDescriptionTextViewHeightActive: true)
    
    
    // feed 2
    let feed2 = FeedModel(imgProfile: #imageLiteral(resourceName: "ronaldo3"), userName: "Cristiano Ronaldo", description: "For UICollectionView however, the solution is a little less obvious. When I was trying to figure it out, a Google search turned up several different solutions, some of which seemed hackier than others. It wasn’t clear to me what the “right” or cleanest way to accomplish dynamically sized cells was, or whether this was even a thing supported by Apple (spoiler alert: it is.)", location: "Funchal, Madeira, Portugal", date: "12 days ago", isLieked: false, likes: 1233, comments: 312, images: [UIImage(named: "mas3")!, UIImage(named: "mas2")!], currentImage: UIImage(named: "mas3")!, descriptionTextVeiwTextContainerMode: .byTruncatingTail, isDescriptionTextViewHeightActive: true)
    
    // feed 3
    let feed3 = FeedModel(imgProfile: #imageLiteral(resourceName: "ronaldo1"), userName: "Cristiano Ronaldo", description: "If your collection view is created by a XIB, you will need to create a property with an IBOutlet and connect it to your collection view’s layout in order to set estimatedItemSize. There should be a Layout placeholder available in Interface Builder for you to control+drag to.", location: "Iran, Abhar", date: "12 days ago", isLieked: false, likes: 1233, comments: 312, images: [UIImage(named: "mas1")!, UIImage(named: "mas3")!], currentImage: UIImage(named: "mas1")!, descriptionTextVeiwTextContainerMode: .byTruncatingTail, isDescriptionTextViewHeightActive: true)
    
    return [feed1, feed2, feed3, feed2]
}


/*
 
 let description1 = ""
 
 let description2 = "If your collection view is created programmatically, you would have had to instantiate a UICollectionViewFlowLayout so you can just set the property on that."
 
 let description3 = "Remember that a constraint is simply a mathematical equation relating one dimension to another."
 
 let description4 = "If your collection view is created by a XIB, you will need to create a property with an IBOutlet and connect it to your collection view’s layout in order to set estimatedItemSize. There should be a Layout placeholder available in Interface Builder for you to control+drag to."
 
 let description5 = "For UICollectionView however, the solution is a little less obvious. When I was trying to figure it out, a Google search turned up several different solutions, some of which seemed hackier than others. It wasn’t clear to me what the “right” or cleanest way to accomplish dynamically sized cells was, or whether this was even a thing supported by Apple (spoiler alert: it is.)"
 
 */
