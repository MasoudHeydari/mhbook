//
//  Size+Home.swift
//  MHBook
//
//  Created by Masoud Heydari on 8/14/19.
//  Copyright © 2019 Masoud Heydari. All rights reserved.
//


import UIKit

/*************************************************/
/*********** Extension for Controller ************/
/*************************************************/
extension Size.Controller {
    
    struct Home {
        // header view size
        static let storyHeaderViewSize: CGSize = {
            let height: CGFloat = (Story.storyCellTopPadding * 2 ) + Story.storyCellSize.height + 1
            let width: CGFloat = Size.Window.aWidth
            return .init(width: width, height: height)
        }()
        
    }
    
    struct Story {
        // story cell size
        static let storyCellSize: CGSize = .init(width: (Size.Window.aHeight / 4 ) * 0.5625, height:  Size.Window.aHeight / 4)
        // cell inset
        static let storyCellTopPadding: CGFloat = 12
        static let storyCellBottomPadding: CGFloat = 12
        static let storyCellInset = UIEdgeInsets(top: storyCellTopPadding, left: 6, bottom: storyCellBottomPadding, right: 6)
        
    }
    
}


/**************************************************/
/**************  Extension for View  **************/
/**************************************************/
extension Size.View {
    
    struct StoryCell {
        
        // label subtitle
        static let txtSubtitleFont: CGFloat = 18
        static let lblSubtitleTopPaddign: CGFloat = 30
        static let lblSubtitleRightPaddign: CGFloat = -25
        static let lblSubtitleLeftPaddign: CGFloat = 25
        
        // image profile
        static let btnImageProfileHeight: CGFloat = Size.Controller.Story.storyCellSize.width * (1.8 / 5)
        static let btnImageProfileWidth: CGFloat = Size.Controller.Story.storyCellSize.width * (1.8 / 5)
        static let btnImageProfileCornerRadius: CGFloat = btnImageProfileWidth / 2
        static let btnImageProfileTopPaddign: CGFloat = 12
        static let btnImageProfileLeftPaddign: CGFloat = 12
        
    }
    
    struct HoemFeedCell {
        
        // image profile
        static let imgProfileHeight: CGFloat = 40
        static let imgProfileWidth: CGFloat = imgProfileHeight
        static let imgProfileCornerRadius: CGFloat = imgProfileWidth / 2

        // image banner
        static let imgBannerHeight: CGFloat = Size.Window.aWidth * 0.5625
        static let imgBannerLeftPadding: CGFloat = 8
        static let imgBannerRightPadding: CGFloat = -8
        
        // button like
        static let btnLikeHeight: CGFloat = 42
        static let btnLikeWidth: CGFloat = 42
        
        // button comment
        static let btnCommentHeight: CGFloat = 42
        static let btnCommentWidth: CGFloat = 42


    }
    
    struct CommentCell {
        
        // image profile
        static let imgProfileHeight: CGFloat = 40
        static let imgProfileWidth: CGFloat = imgProfileHeight
        static let imgProfileCornerRadius: CGFloat = imgProfileWidth / 2
        
    }
    
}
