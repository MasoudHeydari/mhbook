//
//  Color+Home.swift
//  MHBook
//
//  Created by Masoud Heydari on 8/14/19.
//  Copyright © 2019 Masoud Heydari. All rights reserved.
//

import UIKit

/*************************************************/
/*********** Extension for Controller ************/
/*************************************************/
extension Color.Controller {
    
    struct Home {
        static let collectionViewbackground = UIColor(r: 235, g: 235, b: 235)
    }
    
    struct Story {
        static let cellbackground = UIColor(r: 78, g: 116, b: 217)
    }
    
}

/**************************************************/
/**************  Extension for View  **************/
/**************************************************/
extension Color.View {
    
    struct Home {
        static let cellbackground = UIColor(r: 78, g: 116, b: 217)
    }
    
    struct Story {
        static let cellbackground = UIColor(r: 78, g: 116, b: 217)
    }
    
}
