//
//  HomeFeedCell.swift
//  MHBook
//
//  Created by Masoud Heydari on 8/14/19.
//  Copyright © 2019 Masoud Heydari. All rights reserved.
//


import UIKit

class HomeFeedCell: UICollectionViewCell {
    
    var cellDidSelect: (() -> ())?
    var imgProfileDidSelect: (() -> ())?
    var btnMoreDidSelect: (() -> ())?
    var imgBannerDidSelect: (() -> ())?
    var btnLikeDidSelect: (() -> ())?
    var btnCommentDidSelect: (() -> ())?
    var textViewDescriptionDidSelect: (() -> ())?
    
    var descTextViewHeightConstraint: NSLayoutConstraint?
    
    // this is necessary for self sizing
    private lazy var contentViewWidth: NSLayoutConstraint = {
        let contentViewWidth = contentView.widthAnchor.constraint(equalToConstant: bounds.size.width)
        contentViewWidth.isActive = true
        return contentViewWidth
    }()
    
    var collectionViewOffset: CGFloat {
        set { imgBanner.collectionView.contentOffset.x = newValue }
        get { return imgBanner.collectionView.contentOffset.x }
    }
    
    public var feed: FeedModel? {
        didSet {
            if let feed = feed {
                imgProfile.image = feed.imgProfile
                nameLabel.text = feed.userName
                descTextView.textContainer.lineBreakMode = feed.descriptionTextVeiwTextContainerMode
                descTextView.text = feed.description
                locationLabel.text = feed.location
                dateLabel.text = feed.date
                imgBanner.addImages(images: feed.images)
                let btnLikeImg = (feed.isLieked ? #imageLiteral(resourceName: "like") : #imageLiteral(resourceName: "un-like")).withRenderingMode(.alwaysOriginal)
                btnLike.setImage(btnLikeImg, for: .normal)
                likeNumsLabel.text = String(feed.likes)
                commentNumsLabel.text = String(feed.comments)
                descTextViewHeightConstraint?.isActive = feed.isDescriptionTextViewHeightActive
                setupImageBanner()
            }
        }
    }
    
    let imgProfile: UIImageView = {
        let img = UIImageView()
        img.translatesAutoresizingMaskIntoConstraints = false
        img.backgroundColor = UIColor.red.withAlphaComponent(0.5)
        img.layer.cornerRadius = Size.View.HoemFeedCell.imgProfileCornerRadius
        img.clipsToBounds = true
        img.contentMode = .scaleAspectFill
        img.constrainHeight(constant: Size.View.HoemFeedCell.imgProfileHeight)
        img.constrainWidth(constant: Size.View.HoemFeedCell.imgProfileWidth)
        return img
    }()
    
    let nameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Masoud Heydari"
        label.numberOfLines = 1
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.textAlignment = .left
        label.sizeToFit()
        return label
    }()
    
    let locationLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 13)
        label.numberOfLines = 1
        label.text = "Iran, Tehran"
        label.textAlignment = .right
        label.textColor = .lightGray
        label.backgroundColor = .white
        return label
    }()
    
    let descTextView: UITextView = {
        let tv = UITextView()
        tv.translatesAutoresizingMaskIntoConstraints = false
        tv.font = UIFont.systemFont(ofSize: 14)
        tv.textAlignment = .left
        tv.isUserInteractionEnabled = false
        tv.isScrollEnabled = false
        tv.isSelectable = false
        return tv
    }()
    
    let dateLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 13)
        label.numberOfLines = 1
        label.textAlignment = .left
        label.textColor = .lightGray
        label.text = "4 hours ago"
        return label
    }()
    
    let imgBanner: ImageSliderView = {
        let img = ImageSliderView()
        img.translatesAutoresizingMaskIntoConstraints = false
        img.backgroundColor = UIColor.red.withAlphaComponent(0.5)
        img.layer.cornerRadius = 8
        img.clipsToBounds = true
        return img
    }()
    
    let btnMore: UIButton = {
        let btn = UIButton(type: .system)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.setImage(#imageLiteral(resourceName: "more").withRenderingMode(.alwaysOriginal), for: .normal)
        btn.imageView?.tintColor = .lightGray
        btn.sizeThatFits(.zero)
        btn.constrainWidth(constant: 35)
        return btn
    }()
    
    let btnLike: UIButton = {
        let btn = UIButton(type: .system)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.setImage(#imageLiteral(resourceName: "like").withRenderingMode(.alwaysOriginal), for: .normal)
        btn.constrainWidth(constant: Size.View.HoemFeedCell.btnLikeWidth)
        btn.constrainHeight(constant: Size.View.HoemFeedCell.btnLikeHeight)
        return btn
    }()
    
    let likeNumsLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "12"
        label.numberOfLines = 1
        label.font = UIFont.systemFont(ofSize: 16)
        label.textAlignment = .left
        label.sizeToFit()
        return label
    }()
    
    let btnComment: UIButton = {
        let btn = UIButton(type: .system)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.setImage(#imageLiteral(resourceName: "comments").withRenderingMode(.alwaysOriginal), for: .normal)
        btn.constrainWidth(constant: Size.View.HoemFeedCell.btnCommentWidth)
        btn.constrainHeight(constant: Size.View.HoemFeedCell.btnCommentHeight)
        return btn
    }()
    
    let commentNumsLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "123"
        label.numberOfLines = 1
        label.font = UIFont.systemFont(ofSize: 16)
        label.textAlignment = .left
        label.sizeToFit()
        return label
    }()
    
    let btnMoreSV: UIStackView = {
        let sv = UIStackView()
        sv.axis = .vertical
        sv.translatesAutoresizingMaskIntoConstraints = false
        return sv
    }()
    
    let bottomButtonsSV = UIStackView()
    
    let topBtnAndLabelsSV: UIStackView = {
        let sv = UIStackView()
        sv.spacing = 8
        sv.translatesAutoresizingMaskIntoConstraints = false
        return sv
    }()
    
    let locationAndDateSV: UIStackView = {
        let sv = UIStackView()
        sv.spacing = 4
        sv.translatesAutoresizingMaskIntoConstraints = false
        return sv
    }()
    
    let allTopLabelsSV: UIStackView = {
        let sv = UIStackView()
        sv.spacing = 4
        sv.axis = .vertical
        sv.translatesAutoresizingMaskIntoConstraints = false
        return sv
    }()
    
    let mainContainer: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let mainStackView:  UIStackView = {
        let sv = UIStackView()
        sv.spacing = 4
        sv.translatesAutoresizingMaskIntoConstraints = false
        sv.axis = .vertical
        return sv
    }()
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
        setupViewsTapGestures()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setup() {
        backgroundColor = .white
        let bottomButtonsSpacer = SpacerView(space: 20, direction: .horizontal)
        
        descTextView.textContainer.lineBreakMode = NSLineBreakMode.byTruncatingTail
        descTextViewHeightConstraint = descTextView.heightAnchor.constraint(lessThanOrEqualToConstant: 75)
        descTextViewHeightConstraint?.isActive = true
        
        btnMoreSV.addArrangedSubview(views: [btnMore, SpacerView()])
        locationAndDateSV.addArrangedSubview(views: [locationLabel, SpacerView(), dateLabel])
        allTopLabelsSV.addArrangedSubview(views: [nameLabel, locationAndDateSV])
        topBtnAndLabelsSV.addArrangedSubview(views: [imgProfile, allTopLabelsSV, btnMoreSV])
        bottomButtonsSV.addArrangedSubview(views: [btnLike, likeNumsLabel, bottomButtonsSpacer, btnComment, commentNumsLabel, SpacerView()])
        mainStackView.addArrangedSubview(views: [topBtnAndLabelsSV, descTextView, imgBanner, bottomButtonsSV])
        
        setupMainVSAndMainContainer(mainSV: mainStackView, mainContainer: mainContainer)
    }
    
    private func setupViewsTapGestures() {
        addTapGesture(to: self, action: #selector(cellTapped))
        addTapGesture(to: imgProfile, action: #selector(imgProfileTapped))
        addTapGesture(to: btnMore, action: #selector(btnMoreTapped))
        addTapGesture(to: imgBanner, action: #selector(imgBannerTapped))
        addTapGesture(to: btnLike, action: #selector(btnLikeTapped))
        addTapGesture(to: btnComment, action: #selector(btnCommentTapped))
        addTapGesture(to: descTextView, action: #selector(textViewDescriptionTapped))
    }
    
    private func setupMainVSAndMainContainer(mainSV: UIStackView, mainContainer: UIView) {
        mainContainer.addSubview(mainSV)
        mainSV.fillToSuperview(padding: 8)
        
        contentView.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(mainContainer)
        mainContainer.fillToSuperview()
        
    }
    
    private func setupImageBanner() {
        let imgSize = imgBanner.getFirstImage().size
        let height = calculatedImageHeight(imgHeight: imgSize.height , imgWidth: imgSize.width)
        imgBanner.constrainHeight(constant: height)
    }
    
    private func addTapGesture(to view: UIView, action: Selector) {
        let gesture = UITapGestureRecognizer(target: self, action: action)
        view.isUserInteractionEnabled = true
        view.addGestureRecognizer(gesture)
    }
    
    override func systemLayoutSizeFitting(_ targetSize: CGSize, withHorizontalFittingPriority horizontalFittingPriority: UILayoutPriority, verticalFittingPriority: UILayoutPriority) -> CGSize {
        contentViewWidth.constant = frame.width
        
        var estimatedHeight: CGFloat = 0.0
        estimatedHeight = contentView.systemLayoutSizeFitting(CGSize(width: targetSize.width, height: 1)).height
        
        return .init(w: targetSize.width, h: estimatedHeight)
    }
    
    // MARK:-  Objc
    @objc private func cellTapped() {
        self.cellDidSelect?()
    }
    
    @objc private func imgProfileTapped() {
        self.imgProfileDidSelect?()
    }
    
    @objc private func btnMoreTapped() {
        self.btnMoreDidSelect?()
    }
    
    @objc private func imgBannerTapped() {
        self.imgBannerDidSelect?()
    }
    
    @objc private func btnLikeTapped() {
        self.btnLikeDidSelect?()
    }
    
    @objc private func btnCommentTapped() {
        self.btnCommentDidSelect?()
    }
    
    @objc private func textViewDescriptionTapped() {
        
        if descTextView.textContainer.lineBreakMode == .byTruncatingTail {
            print("text view tapped")
            textViewDescriptionDidSelect?()
        }
    }
    
    /******************************************/
    func setImageSliderTage(forItem item: Int) {
        imgBanner.collectionView.tag = item
        // Stops collection view if it was scrolling.
        imgBanner.collectionView.setContentOffset(imgBanner.collectionView.contentOffset, animated: false)
        imgBanner.collectionView.reloadData()
    }
}
