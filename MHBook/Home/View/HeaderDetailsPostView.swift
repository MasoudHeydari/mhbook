//
//  HeaderDetailsPost.swift
//  MHBook
//
//  Created by Masoud Heydari on 8/15/19.
//  Copyright © 2019 Masoud Heydari. All rights reserved.
//

import UIKit

class HeaderDetailsPostView: UICollectionReusableView {
    
    // MARK:- Porperties
    private let homeFeedCell = HomeFeedCell(frame: .zero)
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setup() {
        backgroundColor = .white
        addSubview(homeFeedCell)
        homeFeedCell.feed = getFeedModels()[0]
        homeFeedCell.fillToSuperview()
        
    }
}
