//
//  HeaderStory.swift
//  MHBook
//
//  Created by Masoud Heydari on 8/14/19.
//  Copyright © 2019 Masoud Heydari. All rights reserved.
//

import UIKit

class HeaderStoryView: UICollectionReusableView {
    
    // MARK:- Porperties
    let storyController = StoryController()
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setup() {
        backgroundColor = .white
        guard let storyController = self.storyController.view else { return }
        addSubview(storyController)
        storyController.fillToSuperview()

    }
}
