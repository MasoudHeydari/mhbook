//
//  DetailsPostCell.swift
//  MHBook
//
//  Created by Masoud Heydari on 8/15/19.
//  Copyright © 2019 Masoud Heydari. All rights reserved.
//

import UIKit

class CommentCell: UICollectionViewCell {
    
    // this is necessary for self sizing
    private lazy var cellWidth: NSLayoutConstraint = {
        let width = contentView.widthAnchor.constraint(equalToConstant: bounds.size.width)
        width.isActive = true
        return width
    }()
    
    public var commentTxt: String? {
        didSet {
            if let commentTxt = commentTxt {
                self.descTextView.text = commentTxt
            }
        }
    }
    
    let imgProfile: UIImageView = {
        let img = UIImageView()
        img.backgroundColor = UIColor.red.withAlphaComponent(0.5)
        img.layer.cornerRadius = Size.View.CommentCell.imgProfileCornerRadius
        img.clipsToBounds = true
        img.image = #imageLiteral(resourceName: "mas1")
        img.contentMode = .scaleAspectFill
        img.translatesAutoresizingMaskIntoConstraints = false
        img.constrainHeight(constant: Size.View.CommentCell.imgProfileHeight)
        img.constrainWidth(constant: Size.View.CommentCell.imgProfileWidth)
        return img
    }()
    
    let nameLabel: UILabel = {
        let label = UILabel()
        label.text = "Masoud Heydari"
        label.numberOfLines = 1
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.textAlignment = .left
        label.sizeToFit()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let descTextView: UITextView = {
        let tv = UITextView()
        tv.font = UIFont.systemFont(ofSize: 14)
        tv.textAlignment = .left
        tv.backgroundColor = .clear
        tv.translatesAutoresizingMaskIntoConstraints = false
        tv.isScrollEnabled = false
        tv.isSelectable = false
        return tv
    }()
    
    let labelsContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 10
        view.backgroundColor = UIColor(r: 245, g: 245, b: 245)
        return view
    }()
    
    let mainContainer: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let labelsSV: UIStackView = {
        let sv = UIStackView(axis: .vertical, spacing: 8)
        sv.translatesAutoresizingMaskIntoConstraints = false
        return sv
    }()
    
    let imgProfileSV: UIStackView = {
        let sv = UIStackView(axis: .vertical, spacing: 8)
        sv.translatesAutoresizingMaskIntoConstraints = false
        return sv
    }()
    
    let mainStackView:  UIStackView = {
        let sv = UIStackView()
        sv.spacing = 8
        sv.translatesAutoresizingMaskIntoConstraints = false
        sv.isLayoutMarginsRelativeArrangement = true
        sv.layoutMargins = .init(top: 6, left: 6, bottom: 6, right: 6)
        return sv
    }()
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setup() {
        backgroundColor = .white

        labelsSV.addArrangedSubview(views: [nameLabel, descTextView])
        labelsContainerView.addSubview(labelsSV)
        labelsSV.fillToSuperview(padding: 12)
        
        imgProfileSV.addArrangedSubview(views: [imgProfile, SpacerView()])
        
        mainStackView.addArrangedSubview(views: [imgProfileSV, labelsContainerView])
        
        setupMainVSAndMainContainer(mainSV: mainStackView, mainContainer: mainContainer)
        
    }
    
    private func setupMainVSAndMainContainer(mainSV: UIStackView, mainContainer: UIView) {
        mainContainer.addSubview(mainSV)
        contentView.addSubview(mainContainer)
        contentView.translatesAutoresizingMaskIntoConstraints = false
        
        mainSV.leftAnchor.constraint(equalTo: mainContainer.leftAnchor, constant: 8).isActive = true
        mainSV.rightAnchor.constraint(equalTo: mainContainer.rightAnchor, constant: -8).isActive = true
        mainSV.topAnchor.constraint(equalTo: mainContainer.topAnchor, constant: 8).isActive = true
        mainSV.bottomAnchor.constraint(equalTo: mainContainer.bottomAnchor, constant: -8).isActive = true
        
        mainContainer.leftAnchor.constraint(equalTo: contentView.leftAnchor).isActive = true
        mainContainer.rightAnchor.constraint(equalTo: contentView.rightAnchor).isActive = true
        mainContainer.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
        mainContainer.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
    }
    
    override func systemLayoutSizeFitting(_ targetSize: CGSize, withHorizontalFittingPriority horizontalFittingPriority: UILayoutPriority, verticalFittingPriority: UILayoutPriority) -> CGSize {
        cellWidth.constant = frame.width
        return contentView.systemLayoutSizeFitting(CGSize(width: targetSize.width, height: 1))
    }
}
