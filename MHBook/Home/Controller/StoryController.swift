//
//  StoryController.swift
//  MHBook
//
//  Created by Masoud Heydari on 8/14/19.
//  Copyright © 2019 Masoud Heydari. All rights reserved.
//

import UIKit

class StoryController: BaseCollectionViewController {
    
    // MARK:- Functions
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupCollectionView()
    }
    
    private func setupView() {
        collectionView.backgroundColor = .white
    }
    
    private func setupCollectionView() {
        guard let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout else { return }
        layout.scrollDirection = .horizontal
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.backgroundColor = .white
        collectionView.register(StoryCell.self, forCellWithReuseIdentifier: Const.Controller.Story.storyCellId)
    }
}

/***********************************************/
/*************** COLLECTION VIEW ***************/
/***********************************************/

extension StoryController {
    
    // num of cells
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    // config cell
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let storyCell = collectionView.dequeueReusableCell(withReuseIdentifier: Const.Controller.Story.storyCellId, for: indexPath) as? StoryCell else { return UICollectionViewCell() }
        return storyCell
    }
    
    // cell size
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return Size.Controller.Story.storyCellSize
    }
    
    // collection view inset
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return Size.Controller.Story.storyCellInset
    }
    
}
