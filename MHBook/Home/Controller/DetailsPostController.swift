//
//  DetailsPostController.swift
//  MHBook
//
//  Created by Masoud Heydari on 8/15/19.
//  Copyright © 2019 Masoud Heydari. All rights reserved.
//

import UIKit

class DetailsPostController: BaseCollectionViewController {
    
    let commentList: [String] = {
        
        let description1 = "Often times when using a UITableVie"
        
        let description2 = "If your collection view is created programmatically, you would have had to instantiate a UICollectionViewFlowLayout so you can just set the property on that."
        
        let description3 = "Remember that a constraint is simply a mathematical equation relating one dimension to another."
        
        let description4 = "If your collection view is created by a XIB, you will need to create a property with an IBOutlet and connect it to your collection view’s layout in order to set estimatedItemSize. There should be a Layout placeholder available in Interface Builder for you to control+drag to."
        
        let description5 = "For UICollectionView however, the solution is a little less obvious. When I was trying to figure it out, a Google search turned up several different solutions, some of which seemed hackier than others. It wasn’t clear to me what the “right” or cleanest way to accomplish dynamically sized cells was, or whether this was even a thing supported by Apple (spoiler alert: it is.)"
        
        return [description1, description2, description5, description3, description4]
    }()
    
    override var canBecomeFirstResponder: Bool {
        return true
    }

    var customInputView: UIView!
//    var sendButton: UIButton!
//    var addMediaButtom: UIButton!
    let textField = CustomInputAccessoryView()
    
    override var inputAccessoryView: UIView? {
        
        if customInputView == nil {
            customInputView = CustomView()
            customInputView.backgroundColor = UIColor.groupTableViewBackground
            textField.placeholder = "I'm gonna grow in height."
            textField.font = .systemFont(ofSize: 15)
            textField.layer.cornerRadius = 5
            
            customInputView.autoresizingMask = .flexibleHeight
            
            customInputView.addSubview(textField)
            
            textField.translatesAutoresizingMaskIntoConstraints = false
            textField.maxHeight = 80
            
            textField.leftAnchor.constraint( equalTo: customInputView.leftAnchor, constant: 8).isActive = true
            textField.rightAnchor.constraint( equalTo: customInputView.rightAnchor, constant: -8).isActive = true
            textField.topAnchor.constraint( equalTo: customInputView.topAnchor, constant: 8).isActive = true
            textField.bottomAnchor.constraint( equalTo: customInputView.layoutMarginsGuide.bottomAnchor, constant: -8 ).isActive = true

            textField.v = self.view
            print("v frame \(self.view.frame) \\ \(Size.Window.aHeight)")
        }
        return customInputView
    }
    
    var layout: UICollectionViewFlowLayout = {
        let layout = UICollectionViewFlowLayout()
        let width = UIScreen.main.bounds.size.width
        layout.sectionInsetReference = .fromSafeArea
        layout.estimatedItemSize = CGSize(width: width, height: 10)
        return layout
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupNavBar()
    }
    
    private func setupNavBar() {
        navigationItem.title = Const.Controller.DetailsPost.navTitle
    }
    
    private func setupView() {
        setupCollectionView()
    }
    
    private func setupCollectionView() {
        collectionView.collectionViewLayout = layout
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.backgroundColor = .white
        collectionView.delegate = self
        collectionView.keyboardDismissMode = .interactive
        collectionView.dataSource = self
        collectionView.alwaysBounceVertical = true
        
        // register cell
        collectionView.register(CommentCell.self, forCellWithReuseIdentifier: Const.Controller.DetailsPost.detailsPostCellId)
        collectionView.register(HomeFeedCell.self, forCellWithReuseIdentifier: Const.Controller.DetailsPost.homeFeedCellId)
        
        // register header
        collectionView.register(HeaderDetailsPostView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: Const.Controller.DetailsPost.headerCellId)
        
        collectionView.isDirectionalLockEnabled = false
        
        view.addSubview(collectionView)
        if #available(iOS 11.0, *) {
            let safeArea = view.safeAreaLayoutGuide
            collectionView.rightAnchor.constraint(equalTo: safeArea.rightAnchor).isActive = true
            collectionView.leftAnchor.constraint(equalTo: safeArea.leftAnchor).isActive = true
            collectionView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
            collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        }
        
//        collectionView.scrollToItem(at: <#IndexPath#>, animated: true)
    }
    
    
    @objc func handleSend() {
        print("works")
        self.textField.resignFirstResponder()
    }
    
}

/***********************************************/
/*************** COLLECTION VIEW ***************/
/***********************************************/
extension DetailsPostController {
    
    // cell config
   override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch indexPath.item {
        case 0:
            guard let homeFeedCell = collectionView.dequeueReusableCell(withReuseIdentifier: Const.Controller.Home.homeFeedCellId, for: indexPath) as? HomeFeedCell else { return UICollectionViewCell() }
            homeFeedCell.feed = getFeedModels().first
            return homeFeedCell
        default:
            guard let commentCell = collectionView.dequeueReusableCell(withReuseIdentifier: Const.Controller.DetailsPost.detailsPostCellId, for: indexPath) as? CommentCell else { return UICollectionViewCell() }
            commentCell.commentTxt = commentList[indexPath.item]
            return commentCell
        }
    }
    
    // cell selection
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
    // header view
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        guard let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: Const.Controller.DetailsPost.headerCellId, for: indexPath) as? HeaderDetailsPostView else { return UICollectionReusableView() }
        return header
    }
    
    // num of cells
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return commentList.count
    }
    
    // gape size
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        layout.estimatedItemSize = CGSize(width: view.safeAreaLayoutGuide.layoutFrame.width, height: 10)
        layout.invalidateLayout()
        super.viewWillTransition(to: size, with: coordinator)
    }
}


class CustomView: UIView {
    
    // this is needed so that the inputAccesoryView is properly sized from the auto layout constraints
    // actual value is not important
    
    override var intrinsicContentSize: CGSize {
        return CGSize.zero
    }
}
