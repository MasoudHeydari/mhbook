//
//  MainConfiguration.swift
//  MHBook
//
//  Created by Masoud Heydari on 8/13/19.
//  Copyright © 2019 Masoud Heydari. All rights reserved.
//

import UIKit

struct MainConfig {
    
    static let shared = MainConfig()
    
    private init() { }
    
    let isFirstLunch: Bool = {
        let launchedBefore = UserDefaults.standard.bool(forKey: Const.UserDefault.islanchedBefore)
        if launchedBefore {
            // Not first launch
            return false
        } else {
            // First launch
            return true
        }
    }()
    
    var rootViewController: UIViewController {
        let rootViewController = isFirstLunch ? IntroSliderController(pages: getIntroModel()) : BaseTabBarController()
        return rootViewController
    }
    
    func firstLaunchFinished() {
        UserDefaults.standard.set(true, forKey: Const.UserDefault.islanchedBefore)
    }
}
