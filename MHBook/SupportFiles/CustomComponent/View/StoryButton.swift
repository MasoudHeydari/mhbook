//
//  StoryButton.swift
//  MHBook
//
//  Created by Masoud Heydari on 8/16/19.
//  Copyright © 2019 Masoud Heydari. All rights reserved.
//

import UIKit

class StoryButton: UIButton {
    
    var padding: CGFloat? {
        didSet {
            setup()
        }
    }
    private var image: UIImage? {
        didSet {
            setup()
        }
    }
    
    private var subImabe: UIImageView = {
        let img = UIImageView()
        img.translatesAutoresizingMaskIntoConstraints = false
        return img
    }()
    
    private func setup() {
        removeSubviews()
        backgroundColor = .clear
        guard let image = self.image else { return }
        addSubview(subImabe)
        subImabe.image = image
        subImabe.fillToSuperview(padding: padding ?? 0)
    }
    
    func setImage(image: UIImage) {
        self.image = image
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        subImabe.layer.cornerRadius = frame.size.height / 2 - ( padding ?? 0 )
        subImabe.clipsToBounds = true
    }
    
}
