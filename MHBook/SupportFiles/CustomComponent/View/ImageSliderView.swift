//
//  ImageSliderView.swift
//  MHBook
//
//  Created by Masoud Heydari on 8/18/19.
//  Copyright © 2019 Masoud Heydari. All rights reserved.
//

import UIKit

protocol ImageSliderViewDelegate: class {
    func imageDidChanged(index: Int, image: UIImage)
}

class ImageSliderView: UIView {
    
    weak var delegate: ImageSliderViewDelegate?
    
    private let imageSliderCellId = "imageSliderCellId"
    private var images: [UIImage] {
        didSet {
            commonInit()
            pageControl.numberOfPages = images.count
            collectionView.reloadData()
        }
    }
    
    private var currentIndex: Int = 0
    
    var imageTapped: ((UIImage?, Int) -> ())?
    
    lazy var collectionView: UICollectionView = {
        let cv = UICollectionView(frame: .zero, collectionViewLayout: self.layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.backgroundColor = .white
        cv.collectionViewLayout = layout
        cv.decelerationRate = .fast
        return cv
    }()
    
    private let layout: SnappingCollectionViewFlowLayout = {
        let layout = SnappingCollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        return layout
    }()
    
    private let pageControl: UIPageControl = {
        let page = UIPageControl()
        page.translatesAutoresizingMaskIntoConstraints = false
        page.currentPage = 0
        page.pageIndicatorTintColor = Color.Controller.IntroSlider.pageControlPageIndicator
        page.currentPageIndicatorTintColor = .white
        return page
    }()
    
    init(images: [UIImage]) {
        self.images = images
        super.init(frame: .zero)
        commonInit()
    }
    
    override init(frame: CGRect) {
        self.images = []
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func commonInit() {
        backgroundColor = .white
        setup()
        setupCollectionView()
        setupCollectionViewLayout()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let currentPage = Int(collectionView.contentOffset.x / collectionView.frame.width)
        pageControl.currentPage = currentPage
    }
    
    private func setup() {
        addViews()
        addViewsConstriants()
    }
    
    private func addViews() {
        addSubview(collectionView)
        addSubview(pageControl)
    }
    
    private func addViewsConstriants() {
        collectionView.fillToSuperview()
        
        [pageControl.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -8),
         pageControl.centerXAnchor.constraint(equalTo: centerXAnchor)]
            .forEach{ $0.isActive = true }
    }
    
    private func setupCollectionView() {
        collectionView.backgroundColor = .white
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.backgroundColor = .white
        collectionView.register(ImageSliderCell.self, forCellWithReuseIdentifier: imageSliderCellId)
    }
    
    private func setupCollectionViewLayout() {
        guard let layout = collectionView.collectionViewLayout as? SnappingCollectionViewFlowLayout else { return }
        layout.delegate = self
    }
    
    func addImages(images: [UIImage]) {
        self.images = images
    }
    
    func getFirstImage() -> UIImage {
        guard let firstImage = self.images.first else { return UIImage() }
        return firstImage
    }
}

/***********************************************/
/*************** COLLECTION VIEW ***************/
/***********************************************/
extension ImageSliderView: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    // num of cells
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }
    
    // cell config
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: imageSliderCellId, for: indexPath) as? ImageSliderCell else { return UICollectionViewCell() }
        cell.cellConfig(image: images[indexPath.item])
        
        return cell
    }
    
    // cell selection
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

    }
    
    // cell size
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return .init(w: bounds.width, h: bounds.height)
    }
}

/*************************************************/
/*************** SNAPPING DELEGATE ***************/
/*************************************************/
extension ImageSliderView: SnappingCollectionViewFlowLayoutDelegate {
    func indexForCurrentItem(index: Int) {
        delegate?.imageDidChanged(index: index, image: images[index])
        self.pageControl.currentPage = index
        self.currentIndex = index
    }
}

/*************************************************/
/*************** IMAGE SLIDER CELL ***************/
/*************************************************/
class ImageSliderCell: UICollectionViewCell {
    
    private let imageView: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFill
        return iv
    }()
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setup() {
        addViews()
        addViewsConstraints()
    }
    
    private func addViews() {
        addSubview(imageView)
        
    }
    
    private func addViewsConstraints() {
        imageView.fillToSuperview()
    }
    
    func cellConfig(image: UIImage?) {
        imageView.image = image
    }
    
}
