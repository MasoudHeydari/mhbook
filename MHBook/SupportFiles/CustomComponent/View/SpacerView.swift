//
//  SpacerView.swift
//  MHBook
//
//  Created by Masoud Heydari on 8/12/19.
//  Copyright © 2019 Masoud Heydari. All rights reserved.
//

import UIKit

class SpacerView: UIView {
    
    private var space: CGFloat = 0
    private var direction: Direction = .none
    
    enum Direction {
        case vertical, horizontal, none
    }
    
    init(space: CGFloat, direction: Direction) {
        self.space = space
        self.direction = direction
        super.init(frame: .zero)
        setup()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setup() {
        switch direction {
        case .vertical:
            constrainHeight(constant: self.space)
        case .horizontal:
            constrainWidth(constant: self.space)
        default:
            ()
        }
    }
}
