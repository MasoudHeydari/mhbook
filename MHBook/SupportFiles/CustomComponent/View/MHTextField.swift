//
//  MHTextField.swift
//  MHBook
//
//  Created by Masoud Heydari on 8/12/19.
//  Copyright © 2019 Masoud Heydari. All rights reserved.
//

import UIKit

class MHTextField: UITextField {
    
    let padding: CGFloat
    let customHeight: CGFloat
    
    init(padding: CGFloat, height: CGFloat) {
        self.padding = padding
        self.customHeight = height
        super.init(frame: .zero)
        layer.cornerRadius = customHeight / 2
        backgroundColor = .white
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: padding, dy: 0)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: padding, dy: 0)
    }
    
    override var intrinsicContentSize: CGSize {
        return .init(width: 0, height: self.customHeight)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
