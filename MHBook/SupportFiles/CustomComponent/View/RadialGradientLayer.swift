//
//  asasa.swift
//  MHBook
//
//  Created by Masoud Heydari on 8/17/19.
//  Copyright © 2019 Masoud Heydari. All rights reserved.
//

import UIKit

class RadialGradientLayer: CALayer {
    
    private var gradientRadius: CGFloat = 0
    private var colors = [CGColor]()
    
    init(gradientRadius: CGFloat, colors: [CGColor]) {
        self.gradientRadius = gradientRadius
        self.colors = colors
        super.init()
        self.needsDisplayOnBoundsChange = true
        
    }
    
    required override init() {
        super.init()
        needsDisplayOnBoundsChange = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    required override init(layer: Any) {
        super.init(layer: layer)
    }
    
    
    override func draw(in ctx: CGContext) {
        ctx.saveGState()
        
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let locations: [CGFloat] = [0, 1]
        
        let gradient = CGGradient(colorsSpace: colorSpace, colors: colors as CFArray, locations: locations)
        
        let center = CGPoint(x: bounds.width / 2.0, y: bounds.height / 2.0)
        let option = CGGradientDrawingOptions(rawValue: 0)
        
        ctx.drawRadialGradient(gradient!, startCenter: center, startRadius: bounds.width / 4.0, endCenter: center, endRadius: gradientRadius, options: option)
    }
}

