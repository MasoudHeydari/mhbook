//
//  UIDevice+Helper.swift
//  MHBook
//
//  Created by Masoud Heydari on 8/12/19.
//  Copyright © 2019 Masoud Heydari. All rights reserved.
//

import UIKit

extension UIDevice {
    
    enum DeviceKind {
        case iPad
        case iPhone
    }
    
    static let kind: DeviceKind = {
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.phone {
            return .iPhone
        } else {
            return .iPad
        }
    }()
    
    class var onePixel: CGFloat {
        return CGFloat(1.0) / UIScreen.main.scale
    }
    
    
    static func roundFloatToPixel(_ value: CGFloat) -> CGFloat {
        return round(value * UIScreen.main.scale) / UIScreen.main.scale
    }
}


/********* SCREEN TYPE **********/

extension UIDevice {
    
    var isIPhoneX: Bool {
        return UIScreen.main.nativeBounds.height == 2436
    }
    
    var isIPhone: Bool {
        return UIDevice.current.userInterfaceIdiom == .phone
    }
    
    var isIPad: Bool {
        return UIDevice.current.userInterfaceIdiom == .pad
    }
    
    static let isiPhoneXScreen: Bool = {
        return ScreenType.iPhones_X_XS == screenType
            || ScreenType.iPhone_XSMax == screenType
            || ScreenType.iPhone_XR == screenType
    }()
    
    enum ScreenType: String {
        case iPhones_4_4S = "iPhone 4 or iPhone 4S"
        case iPhones_5_5s_5c_SE = "iPhone 5, iPhone 5s, iPhone 5c or iPhone SE"
        case iPhones_6_6s_7_8 = "iPhone 6, iPhone 6S, iPhone 7 or iPhone 8"
        case iPhones_6Plus_6sPlus_7Plus_8Plus = "iPhone 6 Plus, iPhone 6S Plus, iPhone 7 Plus or iPhone 8 Plus"
        case iPhones_X_XS = "iPhone X or iPhone XS"
        case iPhone_XR = "iPhone XR"
        case iPhone_XSMax = "iPhone XS Max"
        /// sdsd
        case iPad_pro_9_7_mini_air = "iPad Mini 2, iPad Mini 3, iPad Mini 4 || iPad 3, iPad 4, iPad Air, iPad Air 2 || iPad Pro 9.7-inch"
        case ipad_pro_10_5_inch = "iPad Pro 10.5-inch"
        case ipad_pro_11_inch = "iPad Pro 11-inch"
        case ipad_pro_12_9_inch = "iPad Pro 12.9-inch"
        
        case unknown
    }
    
    static var screenType: ScreenType {
        switch UIScreen.main.nativeBounds.height {
        // check for iPhone devices
        case 960:
            return .iPhones_4_4S
        case 1136:
            return .iPhones_5_5s_5c_SE
        case 1334:
            return .iPhones_6_6s_7_8
        case 1792:
            return .iPhone_XR
        case 1920, 2208:
            return .iPhones_6Plus_6sPlus_7Plus_8Plus
        case 2436:
            return .iPhones_X_XS
        case 2688:
            return .iPhone_XSMax
            
        // check for iPad devices
        case 2048:
            return .iPad_pro_9_7_mini_air
        case 2388:
            return .ipad_pro_11_inch
        case 2732:
            return .ipad_pro_12_9_inch
        default:
            return .unknown
        }
    }
}
