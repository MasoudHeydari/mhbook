//
//  CGSize+Helper.swift
//  MHBook
//
//  Created by Masoud Heydari on 8/17/19.
//  Copyright © 2019 Masoud Heydari. All rights reserved.
//

import UIKit

extension CGSize {
    init(w: CGFloat, h: CGFloat) {
        self.init(width: w, height: h)
    }
}
