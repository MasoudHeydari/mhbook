//
//  UIViewController+Helper.swift
//  MHBook
//
//  Created by Masoud Heydari on 8/12/19.
//  Copyright © 2019 Masoud Heydari. All rights reserved.
//

import UIKit

extension UIViewController {
    func hideNavigationBarBottomDivider() {
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
    }
}
