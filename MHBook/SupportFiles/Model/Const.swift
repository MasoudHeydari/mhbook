//
//  Const.swift
//  MHBook
//
//  Created by Masoud Heydari on 8/12/19.
//  Copyright © 2019 Masoud Heydari. All rights reserved.
//

import UIKit

struct Const {
    static let empty = ""
    
    struct Controller { }
    
    struct View { }
    
    struct UserDefault {
        static let islanchedBefore = "islanchedBefore"
    }
}

